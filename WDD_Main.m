% Wigner distribution Deconvolution method to retrive the phase of the electron wave.
% Ptychography phase reconstruction based on J.M. Rodenburg and R.H. Bates,
% Phil. Trans. R. Soc. Lond. A (1992) 339, 521-553
% Script uses a structured array named 'ptycho', which contains the
% information to perform the reconstruction. This variable is given to
% different functions that calculate the necessary steps to perform the
% reconstruction. The final information is stored in variables
% % this function needs comments
% ????
% Explanation of each function is found in the function description.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Select the parameter file to run the reconstruction. This file should
% contain the experimental information and the settings for the
% reconstruction. The default is paramfile.m in the current directory. If
% present this is loaded automatically, if not you are prompted to select a
% parameter file. An example file is given in example_parameter_file.m

[ptycho] = load_parameters();
% this function adds to ptycho:
% ptycho.identifier: string contaning identifier of data.
% ptycho.voltage_kV: number defining acceleration voltage in kV.
% ptycho.wavelength: number defining wavelength in Angstrom
% ptycho.ObjApt_angle: number defining probe convergence angle
% ptycho.rot_angle: number defining rotation angle
% ptycho.pix_size: numbers defining image pixel size x-y in Angstrom
% ptycho.IfCorrectDscan: Flag to correct for Dscan
% ptycho.detbinningFlag: Flag to define is detector is binned
% ptycho.repeat_unit_cell_simulation: Flag to define if the unit cell is
% repeated when using simulations
% ptycho.repeat_number_unit_cell_in_x: number to repeat the unit cell of
% simulations in x direction
% ptycho.repeat_number_unit_cell_in_y: number to repeat the unit cell of
% simulations in y direction
% ptycho.use_padding: Flag to define is zero-padding is used
% ptycho.plot_figures: Flag to define is figures are shown.
% ptycho.dataformat: variable that defines what type of data is going to be
% analysed
% ptycho.pacbedthreshold: Treshold value to select the pacbed bright field
% disk area
% ptycho.varfunctions.load_parameters: Flag to indicate the function has
% been run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load the data. Structured array ptycho should contain variables from the loaded
% parameter file. Variable ptycho.dataformat indicates the type of data
% that will be loaded. Options are:
%           'PN' : for data acquired using PNdetector
%           'Sim': for data that was simulated using software
%           'tif': for data that was acquired in tif
%           'hdf5': for data using HDF5 format (Magnus)
[ptycho] = load_data(ptycho);
% this function adds to ptycho:
% ptycho.probe_range: vector containing the coordinates of the probe scan
% ptycho.m: 4D array that contains the ronchigram for each probe position
% ptycho.varfunctions.load_data: Flag to indicate the function has been run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% If running with simulated data and wish to add poisson noise as
% approprate for a given dose (as specified in parameter file), this will add
% the noise to the CBED images.
if ptycho.add_noise
    ptycho.m = add_noise_to_CBEDs(ptycho.m, ptycho.dose, ptycho.pix_size);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the real space vectors and variables.
[ptycho] = define_real_space(ptycho);
% this function adds to ptycho:
% ptycho.scanwindow: vector with the pixel values of the scanned area
% ptycho.ObjSize: vector with the pixel of the scanned area + zero-padding (if
% stated in parameter file). Padding is useful to avoid artifacts when
% doing FFTs.
% ptycho.X0: vector of probe position coordinates in x
% ptycho.Y0: vector of probe position coordinates in y
% ptycho.Rx: matrix of probe position coordinates in x from meshgrid
% ptycho.Ry: matrix of probe position coordinates in y from meshgrid
% ptycho.Q_x: vector of reciprocal space coordinates with respect to probe
% position in x
% ptycho.Q_y: vector of reciprocal space coordinates with respect to probe
% position in y
% ptycho.Q: matrix of reciprocal space coodinates with respect to probe
% position (Q-space)
% ptycho.varfunctions.define_real_space: Flag to indicate the function has been run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% In case the detector was binned, modify the variables to account for
% this.
if ptycho.detbinningFlag
    [ptycho] = detector_binning(ptycho);
end
% this function returns:
% ptycho.m: m-matrix, in which the ronchigrams have been binned
% ptycho.varfunctions.detector_binning: Flag to indicate the function has been run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define reciprocal space from Ronchigrams
[ptycho] = define_reciprocal_space(ptycho);
% this function adds to ptycho:
% ptycho.pacbed: matrix containing the Position Averaged Convergent Beam
% Electron Diffraction (PACBED) pattern.
% ptycho.s: structured array that contains the centre and radius of the
% BF disc. In case of binned detectors, it outputs the centre, major and
% minor axis length of the ellipsis.
% ptycho.theta_x: vector of scattering angle in x
% ptycho.theta_y: vector of scattering angle in y
% ptycho.tx: matrix of scattering angle in x from meshgrid
% ptycho.ty: matrix of scattering angle in y from meshgrid
% ptycho.theta: matrix of scattering angles
% ptycho.varfunctions.define_reciprocal_space: Flag to indicate the function has been run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate the detector masks to create synthetic BF,IBF,ABF,ADF and DPC
% images
[ptycho] = calculate_detector_masks(ptycho);
% this function adds to ptycho:
% ptycho.maskBF: matrix of mask for Bright Field detector
% ptycho.maskABF: matrix of mask for Annular Bright Field detector
% ptycho.maskIBF: matrix of mask for Incoherent Bright Field detector
% ptycho.maskDF: matrix of mask for Annular Dark Field detector
% ptycho.maskWP: matrix of mask for Weak Phase Ptychography
% ptycho.R11: matrix of mask for DPC detector
% ptycho.R12: matrix of mask for DPC detector
% ptycho.R13: matrix of mask for DPC detector
% ptycho.R14: matrix of mask for DPC detector
% ptycho.R21: matrix of mask for DPC detector
% ptycho.R22: matrix of mask for DPC detector
% ptycho.R23: matrix of mask for DPC detector
% ptycho.R24: matrix of mask for DPC detector
% ptycho.R31: matrix of mask for DPC detector
% ptycho.R32: matrix of mask for DPC detector
% ptycho.R33: matrix of mask for DPC detector
% ptycho.R34: matrix of mask for DPC detector
% ptycho.varfunctions.calculate_detector_masks: Flag to indicate the function has been run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate and show IBF image
[ptycho] = show_synthetic_IBF(ptycho);
% this function adds to ptycho:
% ptycho.IBFimg: matrix of Incoherent Bright Field image
% ptycho.varfunctions.show_synthetic_IBF: Flag to indicate the function has been run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate and show BF image
[ptycho] = show_synthetic_BF(ptycho);
% this function adds to ptycho:
% ptycho.BFimg: matrix of Bright Field image
% ptycho.varfunctions.show_synthetic_BF: Flag to indicate the function has been run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate and show ABF image
[ptycho] = show_synthetic_ABF(ptycho);
% this function adds to ptycho:
% ptycho.ABFimg: matrix of Annular Bright Field image
% ptycho.varfunctions.show_synthetic_ABF: Flag to indicate the function has been run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate and show ADF image
[ptycho] = show_synthetic_DF(ptycho);
% this function adds to ptycho:         
% ptycho.DFimg: matrix of Annular Dark Field image
% ptycho.varfunctions.show_synthetic_DF: Flag to indicate the function has been run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate and show DPC image
[ptycho] = show_synthetic_DPC(ptycho);
% this function adds to ptycho:
% ptycho.R11img: matrix of image for DPC detector
% ptycho.R12img: matrix of image for DPC detector
% ptycho.R13img: matrix of image for DPC detector
% ptycho.R14img: matrix of image for DPC detector
% ptycho.R21img: matrix of image for DPC detector
% ptycho.R22img: matrix of image for DPC detector
% ptycho.R23img: matrix of image for DPC detector
% ptycho.R24img: matrix of image for DPC detector
% ptycho.R31img: matrix of image for DPC detector
% ptycho.R32img: matrix of image for DPC detector
% ptycho.R33img: matrix of image for DPC detector
% ptycho.R34img: matrix of image for DPC detector
% ptycho.varfunctions.show_synthetic_DPC: Flag to indicate the function has been run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate center of mass of ronchigrams
[ptycho] = calculate_center_of_mass(ptycho);
% this function adds to ptycho:
% ptycho.BFDiscComX: matrix of center of mass coordinates in x
% ptycho.BFDiscComY: matrix of center of mass coordinates in y
% ptycho.BFDiscCom: matrix of center of mass coordinates
% ptycho.varfunctions.calculate_center_of_mass: Flag to indicate the function has been run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% If Dscan needs to be fixed:
if ptycho.IfCorrectDscan
    %[ptycho] = perform_d_scan_correction(pytcho); functions not available;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Display ronchigrams for a specific section. This function only shows the
% data, there is no modification or variable adding to ptycho. For
% visualization purposes. Comment or uncomment if you want to use it.
% Displaying bigger areas is slow due to high number of images.
%[ptycho] = display_ronchigrams_within_section(ptycho);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Truncate ronchigrams to be within the specified collection angle
[ptycho] = truncate_ronchigrams_within_collection_angle(ptycho);
% this function adds to ptycho:
% ptycho.m_wp: 4 D m-matrix with truncated ronchigrams
% ptycho.det_range_x: vector with the detector range in x for truncated
% ronchigrams
% ptycho.det_range_y: vector with the detector range in y for truncated
% ronchigrams
% ptycho.varfunctions.truncate_ronchigrams_within_collection_angle: Flag to indicate the function has been run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Since the ronchigrams have been truncated, we need to re-define reciprocal
% space for the truncated detector plane
[ptycho] = define_reciprocal_space_for_truncated_detector_plane(ptycho);
% this function adds to ptycho:
% ptycho.theta_wp: matrix of scattering angles for the truncated
% ronchigrams
% ptycho.phi_wp: matrix of?
% ptycho.Kwp: matrix of reciprocal space vectors for the truncated
% ronchigrams
% ptycho.kx_wp: reciprocal space vector in x for truncated ronchigram
% ptycho.ky_wp: reciprocal space vector in y for truncated ronchigram
% ptycho.varfunctions.define_reciprocal_space_for_truncated_detector_plane: Flag to indicate the function has been run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate matrix G, which is the Fourier transform of M with respect to
% probe position
[ptycho] = FT_from_M_to_G(ptycho);
% use of parallel computing can be beneficial in some cases:
% [ptycho] = parallel_FT_from_M_to_G(ptycho);
% this function adds to ptycho:
% ptycho.mean_m_wp: matrix containing the pacbed of the truncated
% ronchigram
% ptycho.sumI: sum of the total intensity in all ronchigrams and probe positions
% ptycho.G_wp: 4D array that contains the ronchigram for each spatial frecuency of probe position
% ptycho.size4D: vector containing the dimensions of ptycho.G_wp
% ptycho.varfunctions.FT_from_M_to_G: Flag to indicate the function has been run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Single side band reconstruction function
[ptycho] = single_side_band_reconstruction(ptycho);
% this function adds to ptycho:
% ptycho.trotterimgL = trotterimgL;
% ptycho.trotterimgR = trotterimgR;
% ptycho.varfunctions.single_side_band_reconstruction: Flag to indicate the function has been run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define reciprocal space vectors of resampled ronchigram
[ptycho] = define_reciprocal_space_resampled_ronchigram(ptycho);

% ptycho.varfunctions.define_reciprocal_space_resampled_ronchigram: Flag to indicate the function has been run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define probe function
[ptycho] = define_probe_function(ptycho);

% ptycho.varfunctions.define_probe_function: Flag to indicate the function has been run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[ptycho] = FT_from_G_to_H(ptycho);
%[ptycho] =  parallel_FT_from_G_to_H(ptycho);

% ptycho.varfunctions.FT_from_G_to_H: Flag to indicate the function has been run

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Wigner Distribution Deconvolution reconstruction function
[ptycho] = wigner_distribution_deconvolution_reconstruction(ptycho);
% this function adds to ptycho:
% % this function needs comments
%
% ???
% ptycho.varfunctions.wigner_distribution_deconvolution_reconstruction: Flag to indicate the function has been run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




