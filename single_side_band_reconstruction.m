function [ptycho] =  single_side_band_reconstruction(ptycho)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% To perform the single side band reconstruction. 
% It looks at the Kf space (detector plane) at every Q value, and extract
% the phase and amplitude information.
% The algorithm focuses on the overlapping discs between scattered beam and
% central bright discs, therefore frequencies that do not have overlapping
% regions with bright field discs are neglected.
% Function requires structured array ptycho. Figures are displayed if
% ptycho.plot_figures is set to 1. 
% Input variables required are:
% ptycho.scanwindow: vector with the pixel values of the scanned area 
% ptycho.ObjSize: vector with the pixel of the scanned area + zero-padding
% ptycho.ObjApt_angle: Probe convergence angle
% ptycho.rot_angle: Rotation angle 
% ptycho.X0: vector of probe position coordinates in x 
% ptycho.Y0: vector of probe position coordinates in y 
% ptycho.kx_wp: reciprocal space vector in x for truncated ronchigram
% ptycho.ky_wp: reciprocal space vector in y for truncated ronchigram
% ptycho.Q_x: vector of reciprocal space coordinates with respect to probe
% position in x
% ptycho.Q_y: vector of reciprocal space coordinates with respect to probe
% position in y 
% ptycho.Q : matrix of reciprocal space coodinates with respect to probe
% position (Q-space)
% ptycho.G_wp: 4D array that contains the ronchigram for each spatial frecuency of probe position  
% ptycho.IBFimg: matrix of Incoherent Bright Field image  
% ptycho.ABFimg: matrix of Annular Bright Field image
% ptycho.DFimg: matrix of Annular Dark Field image
% Output variables added to ptycho are:
% ptycho.trotterimgL = trotterimgL;
% ptycho.trotterimgR = trotterimgR;
% ptycho.varfunctions.single_side_band_reconstruction: Flag to indicate the function has been run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc
display('running single side band (SSB) reconstruction')

trotterphasesumL = zeros(ptycho.scanwindow);
trotterphasesumR = zeros(ptycho.scanwindow);

for yy=1:ptycho.ObjSize(1)
        display(['SSB: ',num2str(yy/ptycho.ObjSize(1)*100),'%'])
    for xx=1:ptycho.ObjSize(2) 
        
        g =  squeeze(ptycho.G_wp(:,:,yy,xx));

        Q_x_rot = ptycho.Q_x(xx).*cos(ptycho.rot_angle) - ptycho.Q_y(yy).*sin(ptycho.rot_angle);
        Q_y_rot = ptycho.Q_x(xx).*sin(ptycho.rot_angle) + ptycho.Q_y(yy).*cos(ptycho.rot_angle);
        [dx,dy] = meshgrid(ptycho.kx_wp + Q_x_rot , ptycho.ky_wp + Q_y_rot);
        d1 = sqrt(dx.*dx+dy.*dy);
        [dx,dy] = meshgrid(ptycho.kx_wp - Q_x_rot , ptycho.ky_wp - Q_y_rot);
        d2 = sqrt(dx.*dx+dy.*dy);
        [dx,dy] = meshgrid(ptycho.kx_wp, ptycho.ky_wp);
        d3 = sqrt(dx.*dx+dy.*dy);
        
        g_ampL = abs(g);
        g_ampL(d1>ptycho.ObjApt_angle)=0;
        g_ampL(d2<ptycho.ObjApt_angle)=0;
        g_ampL(d3>ptycho.ObjApt_angle)=0;
        g_ampR = abs(g);
        g_ampR(d1<ptycho.ObjApt_angle)=0;
        g_ampR(d2>ptycho.ObjApt_angle)=0;
        g_ampR(d3>ptycho.ObjApt_angle)=0;
        if ptycho.Q(yy,xx) == 0
            g_ampL = abs(g);
            g_ampR = abs(g);
        end
        g_phaseL = angle(g);
        g_phaseL(d1>ptycho.ObjApt_angle)=0;
        g_phaseL(d2<ptycho.ObjApt_angle)=0;
        g_phaseL(d3>ptycho.ObjApt_angle)=0;
        g_phaseR = angle(g);
        g_phaseR(d1<ptycho.ObjApt_angle)=0;
        g_phaseR(d2>ptycho.ObjApt_angle)=0;
        g_phaseR(d3>ptycho.ObjApt_angle)=0;

        trotterL = g_ampL.*exp(sqrt(-1).*(g_phaseL));
        trotterphasesumL(yy,xx) = sum(trotterL(:));
        trotterR = g_ampR.*exp(sqrt(-1).*g_phaseR);
        trotterphasesumR(yy,xx) = sum(trotterR(:));
    end
    clc
end


trotterimgL = fftshift(ifft2(ifftshift(trotterphasesumL)));
trotterimgR = fftshift(ifft2(ifftshift(trotterphasesumR)));

ptycho.trotterimgL = trotterimgL;
ptycho.trotterimgR = trotterimgR;
ptycho.varfunctions.single_side_band_reconstruction = 1;

if ptycho.plot_figures
    show_SSB_results(ptycho);
end

end