function [ptycho] = show_synthetic_ABF(ptycho)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% To calculate and show synthethic Annular Bright Field image. 
% Function requires structured array ptycho. Figures are displayed if
% ptycho.plot_figures is set to 1. 
% Input variables required are:
% ptycho.m: 4D array that contains the ronchigram for each probe position
% ptycho.pacbed: matrix containing the Position Averaged Convergent Beam
% Electron Diffraction (PACBED) pattern.
% ptycho.maskABF: matrix of mask for Incoherent Bright Field detector
% If ptycho.maskABF is not available, it will calculate it. This will require the
% following variables:
%   ptycho.ObjApt_angle: Probe convergence angle
%   ptycho.theta: matrix of scattering angles
% Output variables added to ptycho are:
% ptycho.ABFimg: matrix of Annular Bright Field image
% ptycho.varfunctions.show_synthetic_ABF: Flag to indicate the function has been run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ptycho.varfunctions.show_synthetic_ABF %check if function has been run
    
    figure;
    imagesc(ptycho.ABFimg); axis image; axis off; title('ABF')
    colormap gray
    
else
    if ptycho.varfunctions.calculate_detector_masks
        maskABF = ptycho.maskABF;
    else
        ptycho = calculate_detector_masks(ptycho);
        maskABF = ptycho.maskABF;
    end
    
    % calculate the ABF image
    
    ABFimg = zeros(size(ptycho.m,3),size(ptycho.m,4));
    
    for yy=1:size(ptycho.m,3)
        for xx = 1:size(ptycho.m,4)
            %ABF
            img = ptycho.m(:,:,yy,xx).*maskABF;
            ABFimg(yy,xx) = sum(img(:));
        end
    end
    
    if ptycho.plot_figures
        figure
        imagesc(ptycho.pacbed.*maskABF);axis image;
        title('ABF detector')
        
        figure;
        imagesc(ABFimg); axis image; axis off; title('ABF')
        colormap gray
        colormap gray
    end
    
    ptycho.ABFimg = ABFimg;
    ptycho.varfunctions.show_synthetic_ABF = 1;
    
end


end

